from datetime import datetime


class Date:
    def __init__(self):
        self.current_date = datetime.now()
        self.transformed_current_date = datetime.min

    def get_transformed_current_date(self):
        self.transformed_current_date = self.current_date.date()
        return self.transformed_current_date

    def get_transformed_current_date_year(self):
        transformed_current_year = self.current_date.year
        return transformed_current_year

    def get_transformed_current_date_month(self):
        transformed_current_month = self.current_date.month
        return transformed_current_month

    def get_transformed_current_date_day(self):
        transformed_current_day = self.current_date.day
        return transformed_current_day


if __name__ == '__main__':
    date = Date()

    n = input("Enter number of cases:\n")
    while len(n) == 0:
        n = input("Enter number of cases:\n")
    n = int(n)
    c = 0

    currentDate = date.get_transformed_current_date()

    while n:
        n -= 1
        print("Current date: ")

        print(currentDate)

        birthDate = input("Enter birth date (in format dd/mm/yyyy):\n")
        currentYear = date.get_transformed_current_date_year()
        print(currentYear)
        currentMonth = date.get_transformed_current_date_month()
        currentDay = date.get_transformed_current_date_day()

        birthDay, birthMonth, birthYear = birthDate.split("/")

        year = int(currentYear) - int(birthYear)
        month = int(currentMonth) - int(birthMonth)
        day = int(currentDay) - int(birthDay)

        if month == 0 and day < 0:
            day = -1
        else:
            day = 0

        if month >= 0:
            month = 0
        else:
            month = -1

        res = year + month + day
        c = c + 1

        if res < 0:
            print("Case #" + str(c) + ":", "Invalid birth date (not born yet...)")
        elif res > 130:
            print("Case #" + str(c) + ":", "Check birth date (you are very old...)")
        else:
            print("Case #" + str(c) + ":", res)
